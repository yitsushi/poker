var Card = require('./Card');

var Dealer = (function(){
  var Deck = [];
  var NewDeck = [];
  var Types = ['Spades:black', 'Clubs:black', 'Hearts:red', 'Diamonds:red'];
  var Cards = [ "Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King" ]

  var createCard = function(type, number, name, color) {
    Deck.push({
      type: type,
      number: number,
      name: name,
      color: color
    })
  }

  var randomSort = function(item1, item2) {
      return Math.round(Math.random() * 1000) - Math.round(Math.random() * 1000);
  }

  var shuffle = function() {
    NewDeck = Deck.sort(randomSort);
    for (var i = 0; i < Deck.length - 1; i++) {
      NewDeck = NewDeck.sort(randomSort);
    }

    return true;
  };

  var pickOne = function() {
    return NewDeck.pop();
  };

  // Init
  for (var i = 0; i < Types.length; i++) {
    for (var j = 0; j < Cards.length; j++) {
      var t = Types[i].split(/:/);
      Deck.push(new Card(t[0], j + 1, Cards[j], t[1]));
//      createCard(t[0], j + 1, Cards[j], t[1]);
    }
  }

  return {
    shuffle: shuffle,
    pickOne: pickOne
  }
}());

module.exports = Dealer;
