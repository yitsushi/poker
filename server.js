var Dealer = require('./Dealer');

var P1 = [], P2 = [];

Dealer.shuffle();

for (var i = 0; i < 5; i++) {
  P1.push(Dealer.pickOne());
  P2.push(Dealer.pickOne());
}

console.log('Player#1');
for (var i = 0; i < P1.length; i++) {
  console.log("  " + P1[i].toString());
}
console.log('Player#2');
for (var i = 0; i < P2.length; i++) {
  console.log("  " + P2[i].toString());
}
