var Card = function(type, number, name, color) {
  this.type = type;
  this.number = number;
  this.name = name;
  this.color = color;
};
Card.prototype.toString = function() {
  return this.type + " " + this.name + " (" + this.color + ")";
};

module.exports = Card;
